import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import ScanBarcode from 'react-native-scan-barcode';
import Constants from 'expo-constants';
 
export default class RNCameraScreen extends Component {
   constructor(props) {
    super(props);
    this.state = {
      torchMode: 'off',
      cameraType: 'back',
    };
   
   }
  barcodeReceived(e) {
    alert(`Bar code with type ${e.type} and data ${e.data} has been scanned!`);
    // console.log('Barcode: ' + e.data);
    // console.log('Type: ' + e.type);
  }
 
  render() {
    return (
      <ScanBarcode
        onBarCodeRead={this.barcodeReceived}
        style={{ flex: 1 }}
        torchMode={this.state.torchMode}
        cameraType={this.state.cameraType}
      />
    );
  }
}
 
//AppRegistry.registerComponent('RNCameraScreen', () => RNCameraScreen);