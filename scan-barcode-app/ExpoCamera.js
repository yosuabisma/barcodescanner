import React, { Component } from 'react';
import { Vibration, Text, View, StyleSheet, Alert, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import Constants from 'expo-constants';
// import { createStackNavigator, createAppContainer } from 'react-navigation';
import * as Permissions from 'expo-permissions';

export default class ExpoCamera extends Component {
  constructor(props){
    super(props);
    this.state = { 
        hasCameraPermission: null,
        scanned: false,
        dataBarcode: null,
        barcodeData: "",
        barcodeType: ""
    };
  }
  // state = {
  //   hasCameraPermission: null,
  //   scanned: false,
  //   dataBarcode: null,
  // };
  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-end',
        }}>
            <BarCodeScanner
              onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
              style={StyleSheet.absoluteFillObject}
            />
          

        {scanned && (
          <View>
            <View style={{color:'#ffffff', backgroundColor: '#f51b00', textAlign: 'center', padding: 10}}>
              <View>
                <Text>
                    Type : {this.state.barcodeType}
                </Text>
              </View>
              <View>
                <Text>
                    Data : {this.state.barcodeData}
                </Text>
              </View>
            </View>
            <Button title={'Tap to Scan Next Barcode'} onPress={() => this.setState({ scanned: false })} />
          </View>
          
        )}
      </View>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    this.setState({
      barcodeType : JSON.stringify(type),
      barcodeData : JSON.stringify(data)
    });
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    Vibration.vibrate(100);
  };
}
