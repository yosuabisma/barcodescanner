import React, { Component } from 'react';
//import { AppNavigator } from './AppNavigator';
import Home from './Home';
import ExpoCamera from './ExpoCamera';
// import RNCameraScreen from './RNCameraScreen';
// import BarcodeScanCamera from './BarcodeScanCamera';
import {  createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

const RootStack = createStackNavigator(
  {
    Home: Home,
    ExpoCamera: ExpoCamera,
    // RNCameraScreen: RNCameraScreen,
    // BarcodeScanCamera: BarcodeScanCamera,
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return (
      // <AppNavigator/>
      // <Home/>
      <AppContainer/>
    );
  }
}