import React, { Component } from 'react';
import { Vibration, Text, View, StyleSheet, Alert, Button } from 'react-native';
import Constants from 'expo-constants';

export default class Home extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
          <View style={{marginBottom: 30}}>
            <Text style={{fontSize: 30}}>Welcome!</Text>
          </View>
          <View style={{marginBottom: 30}}>
            <Button title="Expo Barcode Scanner" onPress={() => this.props.navigation.navigate('ExpoCamera')} />
          </View>
          {/* <View style={{marginBottom: 30}}>
            <Button title="React Native Scan Barcode" onPress={() => this.props.navigation.navigate('RNCameraScreen')}/>
          </View>
          <View>
            <Button title="React Native Camera" onPress={() => this.props.navigation.navigate('BarcodeScanCamera')}/>
          </View> */}
      </View>
    );
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  }
});
