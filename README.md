## Install
```
expo install expo-barcode-scanner
```
atau
```
npm install expo-barcode-scanner
```

## Run App
```
npm start
```

## Open App in Expo
```
exp://<Your IP Adress>:19000
```
